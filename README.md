# FlickrPhotoSearchDemo
> FlickrPhotoSearchDemo 是一個根據 Flickr API 所開發的 iOS Demo App，實現 Flickr 公開圖片搜尋功能，搜尋結果將以一個可無限滑動的 CollectionView 頁面顯示。此 App 僅供學習與交流使用。

[![Swift Version][swift-image]][swift-url]

### 開發環境

- XCode 11.3+
- Swift 5.1+
- iOS 13.0+

### 功能介紹

-  [x] 第一頁為搜尋條件頁，需輸入關鍵字 `(text)` 與每次搜尋數量 `(per_page)`
-  [x] 按下搜尋後，會基於條件顯示搜尋結果 `(圖片+標題)`
-  [x] 搜尋結果頁可無限滑動，每次依據搜尋數量載入新的資料
-  [x] 已載入的圖片使用 `NSCache` 緩存，供下次搜尋時使用
-  [x] 所有輸入框皆具防呆檢核
-  [ ] 我的最愛與本地儲存

### Flickr API Documentation

圖片獲取方式使用 [Flickr Search API](https://www.flickr.com/services/api/flickr.photos.search.html)
- 注意 Flickr Token 須自行產生且有時效性

### 👤 聯絡方式
yosh - joey.yu.tw@gmail.com

[https://gitlab.com/yoshLab/flickrphotosearchdemo](https://gitlab.com/yoshLab/flickrphotosearchdemo)

[swift-image]:https://img.shields.io/badge/swift-5.1-orange.svg
[swift-url]: https://swift.org/