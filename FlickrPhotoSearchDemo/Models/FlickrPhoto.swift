//
//  FlickrPhoto.swift
//  TestFlickr
//
//  Created by TWDANMACBOOK-2 on 2020/3/7.
//  Copyright © 2020 TWDANMACBOOK-2. All rights reserved.
//

import Foundation
import UIKit

struct FlickrPhoto: Codable {
    
    let id: String
    let owner: String
    let secret: String
    let server: String
    let title: String
    let farm: Int
    let isfamily: Int
    let isfriend: Int
    let ispublic: Int
    
    var photoUrl: String {
        let urlString = String(format: Keys.standard.FlickrImageUrl, farm, server, id, secret)
        return urlString
    }
}
