//
//  FlickrPhotoCategory.swift
//  TestFlickr
//
//  Created by TWDANMACBOOK-2 on 2020/3/7.
//  Copyright © 2020 TWDANMACBOOK-2. All rights reserved.
//

import Foundation
import UIKit

struct FlickrPhotoSearchResult: Codable {
    let stat: String
    let photos: FlickrPhotoCategory?
}

struct FlickrPhotoCategory: Codable {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let photo: [FlickrPhoto]
}
