//
//  ImageLoader.swift
//  TestFlickr
//
//  Created by TWDANMACBOOK-2 on 2020/3/7.
//  Copyright © 2020 TWDANMACBOOK-2. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    func loadImage(withUrl urlString: String, enableCache: Bool = true) {
        guard let url = URL(string: urlString) else { return }
        self.image = nil
        
        if enableCache
        {
            if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
                self.image = cachedImage
                return
            }
        }
        
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView.init(style:.medium)
        DispatchQueue.main.async { [weak activityIndicator, weak self] in
            guard let activityIndicator = activityIndicator, let self = self else { return }
            self.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            activityIndicator.center = self.center
        }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                return
            }
            DispatchQueue.main.async { [weak activityIndicator, weak self] in
                guard let activityIndicator = activityIndicator, let self = self else { return }
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                    activityIndicator.removeFromSuperview()
                }
            }
        }).resume()
    }
}
