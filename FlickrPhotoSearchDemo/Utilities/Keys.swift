//
//  Keys.swift
//  TestFlickr
//
//  Created by TWDANMACBOOK-2 on 2020/3/7.
//  Copyright © 2020 TWDANMACBOOK-2. All rights reserved.
//

import Foundation

struct Keys {
    
    let PhotoCellId = "PhotoCell"
    let FlickrPhotoSearchUrl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=a1f842903ccde97802ee96c7d02edf96&format=json&nojsoncallback=1&text=%@&per_page=%ld&page=%ld"
    let FlickrImageUrl = "https://farm%d.staticflickr.com/%@/%@_%@_m.jpg"
    let PreloadNumber = 8
    static let standard: Keys = {
        return Keys()
    }()
}
