//
//  PhotoListCollectionViewController.swift
//  TestFlickr
//
//  Created by TWDANMACBOOK-2 on 2020/3/7.
//  Copyright © 2020 TWDANMACBOOK-2. All rights reserved.
//

import UIKit

class PhotoListCollectionViewController: UICollectionViewController {

    var text:String!
    var perpage:Int!
    fileprivate var page:Int = 1
    fileprivate var total_page:Int = 0
    fileprivate var searchPhotos = [FlickrPhoto]()
    fileprivate var didReceiveSearchResult = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let text = text else {
            return
        }
        self.title = "搜尋結果 " + text
        self.fetchResults()
    }

    private func fetchResults() {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let urlString = String(format: Keys.standard.FlickrPhotoSearchUrl, text, perpage, page).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        guard let reqConfig = Request.init(requestMethod: .get, urlString: urlString) else { return }
        
        NetworkManager.shared.request(reqConfig) { (result) in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            switch result {
            case .Success(let responseData):
                do {
                    //                if let returnData = String(data: responseData, encoding: .utf8) {
                    //                    print(returnData)
                    //                }
                    self.didReceiveSearchResult = true
                    let responseModel = try JSONDecoder().decode(FlickrPhotoSearchResult.self, from: responseData)
                    if responseModel.stat.uppercased().contains("OK") {
                        self.total_page = responseModel.photos?.pages ?? 0
                        self.updateSearchResult(with: responseModel.photos?.photo ?? Array<FlickrPhoto>())
                        return
                    }
                } catch { }
                
                DispatchQueue.main.async {
                   let controller = UIAlertController(title: "Oops", message: NetworkManager.errorMessage, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    controller.addAction(okAction)
                    self.present(controller, animated: true, completion: nil)
                }
                
            default:
                DispatchQueue.main.async {
                    let controller = UIAlertController(title: "Oops", message: NetworkManager.noInternetConnection, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    controller.addAction(okAction)
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func fetchNewPage() {
        if page < total_page {
            page += 1
            self.fetchResults()
        }
    }
    
    private func updateSearchResult(with photo: [FlickrPhoto]) {
        DispatchQueue.main.async { [unowned self] in
            
            let newItems = photo
            self.searchPhotos.append(contentsOf: newItems)
            self.collectionView.reloadData()
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if searchPhotos.count == 0 {
            if didReceiveSearchResult {
                collectionView.setEmptyView(title: "很抱歉，未找出符合「" + text + "」的相關圖片", message: "請返回後以其他關鍵字進行搜尋")
            }
            else {
                collectionView.setEmptyView(title: "載入中...", message: "請稍候，圖片結果馬上呈現")
            }
        }
        else {
            collectionView.restore()
        }
        
        return searchPhotos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Keys.standard.PhotoCellId, for: indexPath) as! PhotoCell
        guard searchPhotos.count != 0 else {
            return cell
        }
        let model = searchPhotos[indexPath.row]
        cell.photoTitle.text = model.title
        cell.photoImageView.loadImage(withUrl: model.photoUrl)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == max(0, searchPhotos.count - Keys.standard.PreloadNumber) {
            fetchNewPage()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionWidth = collectionView.bounds.width;
        var itemWidth = collectionWidth / 2;
        
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            itemWidth = collectionWidth / 3;
        }
        return CGSize(width: itemWidth, height: itemWidth);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
