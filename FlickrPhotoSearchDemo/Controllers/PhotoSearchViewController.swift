//
//  PhotoSearchViewController.swift
//  TestFlickr
//
//  Created by TWDANMACBOOK-2 on 2020/3/7.
//  Copyright © 2020 TWDANMACBOOK-2. All rights reserved.
//

import UIKit

class PhotoSearchViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var SearchText: UITextField!
    @IBOutlet weak var SearchPerPage: UITextField!
    @IBOutlet weak var SearchButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        enableSearch(false)
        SearchText.addTarget(self, action: #selector(actionTextFieldIsEditingChanged), for: UIControl.Event.editingChanged)
        SearchPerPage.addTarget(self, action: #selector(actionTextFieldIsEditingChanged), for: UIControl.Event.editingChanged)
    }
    
    @objc func actionTextFieldIsEditingChanged(sender: UITextField) {
        enableSearch(!SearchText.text!.isEmpty && !SearchPerPage.text!.isEmpty)
    }
    
    fileprivate func enableSearch(_ enable: Bool)
    {
        if enable {
             SearchButton.isEnabled = true
             SearchButton.backgroundColor = .init(red: 0, green: 122/255, blue: 1, alpha: 1)
         }
         else {
             SearchButton.isEnabled = false
             SearchButton.backgroundColor = .lightGray
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let target = segue.destination as! PhotoListCollectionViewController
        target.text = self.SearchText.text
        target.perpage =  Int(self.SearchPerPage?.text ?? "0")
//        target.title = Keys.standard.SearchResult + self.SearchText.text
    }
 

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField === self.SearchPerPage {
            let allowedCharacters = CharacterSet.decimalDigits
            return allowedCharacters.isSuperset(of: CharacterSet(charactersIn: string))
        }
        return true
    }
}
